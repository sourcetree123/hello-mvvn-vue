let uid = 0;
let waiting = false;
let pending = false;
const callbacks = [];
const watcherQueue = [];
const watcherMap = new Map();

class Vue extends Object {
    constructor(options) {
        super();
        this._data = options.data;
        this.observer(this._data);
        // 在vue实例初始化的时候创建watcher，这个watcher的含义是观察这个组件。
        new Watcher(options.el); // watcher观察者，Dep.target指向watcher实例。
    }

    observer(item) {
        if (!item || typeof item != "object") {
            return;
        }
        Object.keys(item).forEach((key) => {
            this.defineReactive(item, key, item[key]);
        });
    }

    defineReactive(obj, prop, value) {
        const dep = new Dep(); // prop的依赖收集器，每一个数据属性都有一个依赖与之对应。

        Object.defineProperty(obj, prop, {
            configurable: true,
            enumerable: true,
            set: function reactiveSetter(newVal) {
                if (parseInt(value) === parseInt(newVal)) {
                    return;
                }
                dep.notify(parseInt(newVal));
            },
            get: function reactiveGetter() {
                dep.addSub(Dep.target); // target是一个Watcher实例。
                return parseInt(value);
            }
        });
    }
}

class VNode extends Object {
    // VNode 归根结底就是一个 JavaScript 对象。
    // 用对象属性来描述DOM节点。
    constructor(tag, data, text, children, elem) {
        super();
        this.tag = tag;   // 模拟的元素的标签名
        this.data = data; // 元素依赖的数据
        this.text = text; // 标签文本
        this.elem = elem; // 虚拟节点对应的真实的Dom节点。
        this.children = children; // 当前节点的子节点列表。
    }
}

class Dep extends Object {
    constructor() {
        super();
        this.subs = [];
    }
    addSub(sub) {
        this.subs.push(sub);
    }
    notify(value) {
        this.subs.forEach(elem => {
            elem.update(value);
        });
    }
}
Dep.target = null;

class Watcher extends Object {
    constructor(el) {
        super();
        Dep.target = this;
        this.$el = el;
        this.id = uid++;
    }

    update() {
        const el = this.$el;
        // this.refresh();
        // 进入异步更新队列
        enqueue(this);
    }

    enqueue(watcher) {
        // 剔除相同的watcher。
        const wid = watcher.id;
        if (watcherMap.has(wid)) {
            return;
        }
        watcherQueue.push(watcher);
        watcherMap.set(wid, true);
        if (!waiting) {
            waiting = true;
            nextTick(flushWatchers());
        }
    }

    refresh(value) {
        console.log('go to patch...');
        const el = this.$el;
        const target = document.getElementById(el);
        if (parseInt(value) === 1) {
            target.style.backgroundColor = "#000";
        } else {
            target.style.backgroundColor = "#fff";
        }
    }
}

// 异步更新队列
function nextTick(cb) {
    // 事件处理函数
    callbacks.push(cb);
    if (!pending) {
        pending = true;
        setTimeout(flushCallbacks, 0);
    }
}

function flushCallbacks() {
    pending = false;
    const copies = callbacks.slice(0);
    callbacks.length = 0;
    for(let i = 0; i < copies.length; i++) {
        copies[i]();
    }
}

function flushWatchers() {
    const N = watcherQueue.length;
    for(let i = 0; i < N; i++) {
        const watcher = watcherQueue[i];
        // 重置watcher
        watcherMap.set(watcher.id, null);
        watcher.refresh();
    }
    waiting = false;
}

function createEmptyVNode() {
    const node = new VNode();
    node.text = '';
    return node;
}

function createTextVNode(text) {
    return new VNode(undefined, undefined, text);
}

function cloneVNode(node) {
    return new VNode(node.tag, node.data, node.text, node.elem, node.children);
}

const common = {
    color: 0
}

window.onload = function() {
    const app = new Vue({
        data: common,
        el: 'el'
    });
    // get方法调用的时机是在render方法执行时。此处，通过打印语句模拟render。
    // get执行时，将watcher添加到依赖收集器中。
    // render过程中访问了数据的某个属性，那么说明该视图的渲染依赖于该属性。
    console.log('color: ', app._data.color);
    const control = document.getElementById('control');
    control.addEventListener('click', function(evt) {
        // set方法调用时，依赖收集器通知所有的watcher去更新视图。
        common.color = evt.target.dataset.color;
    });
}
